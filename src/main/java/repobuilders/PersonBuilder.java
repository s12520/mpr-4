package repobuilders;

import java.sql.ResultSet;
import java.sql.SQLException;

import repositories.IEntityBuilder;
import domain.Person;

public class PersonBuilder implements IEntityBuilder<Person>{

	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();
		person.setFirstName(rs.getString("name"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("pesel"));
		person.setEmail(rs.getString("email"));
		person.setPhoneNumber(rs.getString("phonenumber"));
		person.setId(rs.getInt("id"));
		return person;
	}

}