package domain;


import java.util.*;

public class Role extends Entity {

	private int id;
	private String name;
	
	private List<User> users;
	private List<Permission> permissions;
	private List<Privilege> privileges;
	
	public Role()
	{
		privileges=new ArrayList<Privilege>();
		users= new ArrayList<User>();
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public List<Permission> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
	public List<Privilege> getPrivileges() {
		return privileges;
	}
	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	
	
}
