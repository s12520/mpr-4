package repoimplementation;

import java.sql.Connection;

import domain.Person;
import domain.Role;
import repobuilders.PersonBuilder;
import repobuilders.UserBuilder;
import repositories.IRepository;
import repositories.IRepositoryCatalog;
import repositories.IUserRepository;
import unitofwork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	public IUserRepository getUsers() {
		return new UserRepository(connection, new UserBuilder(), uow);
	}


	public IRepository<Role> getRoles() {
		return null;
	}

	public void commit() {
		uow.commit();
	}

	public IRepository<Person> getPersons() {
		return new PersonRepository(connection, new PersonBuilder(), uow); 
	}

}