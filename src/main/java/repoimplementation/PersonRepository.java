package repoimplementation;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import domain.Person;
import repositories.IEntityBuilder;
import unitofwork.IUnitOfWork;

public class PersonRepository 
	extends Repository<Person>{

	protected PersonRepository(Connection connection,
			IEntityBuilder<Person> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
	}
	
	SimpleDateFormat format= new SimpleDateFormat("dd-MM-yyyy");

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getEmail());
		update.setString(5, entity.getPhoneNumber());
		update.setInt(6, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getPesel());
		insert.setString(4, entity.getEmail());
		insert.setString(5, entity.getPhoneNumber());

	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "update person set (name,surname,pesel,email,nip,dob)=(?,?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into person(name,surname,pesel,email,nip,dob)=(?,?,?,?,?,?)";
	}

	
	
}